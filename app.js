var app = app || {};

(function (app) {
	var Quaternion = function (w, x, y, z) {
		this.w = w || 0;
		this.x = x || 0;
		this.y = y || 0;
		this.z = z || 0;
	}
	Quaternion.prototype.conjugate = function () {
		return new Quaternion(this.w, -this.x, -this.y, -this.z);
	}
	Quaternion.prototype.scale = function (s) {
		return new Quaternion(this.w * s, this.x * s, this.y * s, this.z * s);
	}
	Quaternion.prototype.normalize = function () {
		var n = Math.sqrt(this.x * this.x + this.y * this.y + this.z * this.z + this.w * this.w);
		return new Quaternion(this.w / n, this.x / n, this.y / n, this.z / n);
	}
	Quaternion.prototype.mul = function (other) {
		var x = this.x * other.w + this.y * other.z - this.z * other.y + this.w * other.x;
		var y = -this.x * other.z + this.y * other.w + this.z * other.x + this.w * other.y;
		var z = this.x * other.y - this.y * other.x + this.z * other.w + this.w * other.z;
		var w = -this.x * other.x - this.y * other.y - this.z * other.z + this.w * other.w;
		return new Quaternion(w, x, y, z);
	}
	Quaternion.prototype.add = function (other) {
		return new Quaternion(this.w + other.w, this.x + other.x, this.y + other.y, this.z + other.z);
	}
	Quaternion.prototype.subtract = function (other) {
		return new Quaternion(this.w - other.w, this.x - other.x, this.y - other.y, this.z - other.z);
	}

	app.Quaternion = Quaternion;
})(app);

(function (app) {
	var degtorad = Math.PI / 180; // Degree-to-Radian conversion

	var Orientation = function (alpha, beta, gamma) {
		this.alpha = alpha || 0;
		this.beta = beta || 0;
		this.gamma = gamma || 0;
	}
	Orientation.prototype.asQuaternion = function () {
		var _x = this.beta * degtorad; // beta value
		var _y = this.gamma * degtorad; // gamma value
		var _z = this.alpha * degtorad; // alpha value

		var cX = Math.cos(_x / 2);
		var cY = Math.cos(_y / 2);
		var cZ = Math.cos(_z / 2);
		var sX = Math.sin(_x / 2);
		var sY = Math.sin(_y / 2);
		var sZ = Math.sin(_z / 2);

		var w = cX * cY * cZ - sX * sY * sZ;
		var x = sX * cY * cZ - cX * sY * sZ;
		var y = cX * sY * cZ + sX * cY * sZ;
		var z = cX * cY * sZ + sX * sY * cZ;

		return new app.Quaternion(w, x, y, z);
	}
	app.Orientation = Orientation;
})(app);

(function (app) {

	var _handlers = [];

	var startPoint = {
		x : 0,
		y : 0
	}

	function onMouseDown(event) {
		event.preventDefault();

		startPoint = {
			x : event.clientX,
			y : event.clientY
		};

		document.addEventListener('mousemove', onMouseMove, false);
		document.addEventListener('mouseup', onMouseUp, false);
	}

	function onMouseMove(event) {
		handleRotation(startPoint.x, startPoint.y, event.x, event.y);
		startPoint = {
			x : event.x,
			y : event.y
		}
	}

	function onMouseUp(event) {
		document.removeEventListener('mousemove', onMouseMove, false);
		document.removeEventListener('mouseup', onMouseUp, false);
	}

	function handleRotation(startX, startY, endX, endY) {
		var deltaX = endX - startX;
		var deltaY = endY - startY;
		var speed = 0.01;
		var len = Math.sqrt(deltaX * deltaX + deltaY * deltaY);

		var q = new app.Quaternion(1, speed * deltaY, speed * deltaX, 0).normalize();
		for (var index = 0; index < _handlers.length; ++index) {
			_handlers[index](q);
		}
	}

	document.addEventListener('mousedown', onMouseDown, false);

	var MouseMove = function (w, x, y, z) {
		q = new app.Quaternion(w, x, y, z);

		this.quaternion = function () {
			return q;
		}
		_handlers.push(function (rotate) { //todo: unsubscribe
			q = rotate.mul(q).normalize();
		});
	}

	app.MouseMove = MouseMove;
})(app);

(function (app) {
	var deviceorientation = 'deviceorientation';
	var devicemotion = 'devicemotion';

	var orientation = new app.Orientation();

	var _handlers = {
		deviceorientation : [],
		devicemotion : []
	};
	function subscribe(ev, cb) {
		_handlers[ev].push(cb);
	}
	function unsubscribe(ev, cb) {
		var index = _handlers[ev].indexOf(cb);
		if (index > -1) {
			_handlers[ev].splice(index, 1);
		}
	}
	function trigger(ev, data) {
		for (var index = 0; index < _handlers[ev].length; ++index) {
			_handlers[ev][index](data);
		}
	};

	var DeviceController = function () {};
	DeviceController.prototype.setOrientation = function (alpha, beta, gamma) {
		orientation = new app.Orientation(alpha, beta, gamma);
		trigger(deviceorientation);
	};
	DeviceController.prototype.onOrientation = function (cb) {
		subscribe(deviceorientation, cb)
	};
	DeviceController.prototype.offOrientation = function (cb) {
		unsubscribe(deviceorientation, cb)
	};
	DeviceController.prototype.onMotion = function (cb) {
		subscribe(devicemotion, cb)
	};
	DeviceController.prototype.offMotion = function (cb) {
		unsubscribe(devicemotion, cb)
	};
	DeviceController.prototype.setMotion = function (data) {
		trigger(devicemotion, data);
	};
	DeviceController.prototype.getOrientation = function () {
		return orientation;
	};

	app.DeviceController = new DeviceController();

	window.addEventListener(deviceorientation, function (event) {
		app.DeviceController.setOrientation(event.alpha, event.beta, event.gamma);
	});

	window.addEventListener(devicemotion, function (event) {
		app.DeviceController.setMotion(event);
	});
})(app);

(function (app) {
	var angles = document.querySelector('.angles');
	var quaternion = document.querySelector('.quaternion');
	var motion = document.querySelector('.motion');

	app.DeviceController.onOrientation(handleOrientation);

	function handleOrientation() {
		var orientation = app.DeviceController.getOrientation();
		angles.innerHTML = "alpha: " + orientation.alpha + "\n";
		angles.innerHTML += "beta : " + orientation.beta + "\n";
		angles.innerHTML += "gamma: " + orientation.gamma + "\n";

		var q = orientation.asQuaternion();
		quaternion.innerHTML = "w: " + q.w + "\n";
		quaternion.innerHTML += "x: " + q.x + "\n";
		quaternion.innerHTML += "y: " + q.y + "\n";
		quaternion.innerHTML += "z: " + q.z + "\n";
	}
})(app);

(function (app) {
	var camera,
	scene,
	renderer,
	cube,
	mousemove;

	function init() {
		var width = 500; //window.innerWidth;
		var height = 500; //window.innerHeight;

		camera = new THREE.PerspectiveCamera(70, width / height, 1, 1000);
		camera.position.z = 500;

		scene = new THREE.Scene();

		var geometry = new THREE.BoxGeometry(200, 200, 200);
		for (var i = 0; i < geometry.faces.length; i += 2) {
			var color = {
				h : (1 / (geometry.faces.length)) * i,
				s : 0.5,
				l : 0.5
			};
			geometry.faces[i].color.setHSL(color.h, color.s, color.l);
			geometry.faces[i + 1].color.setHSL(color.h, color.s, color.l);
		}

		var material = new THREE.MeshBasicMaterial({
				vertexColors : THREE.FaceColors,
				overdraw : 0.5
			});

		cube = new THREE.Mesh(geometry, material);
		scene.add(cube);

		renderer = new THREE.WebGLRenderer();
		renderer.setClearColor(0xf0f0f0);
		renderer.setSize(width, height);

		document.body.appendChild(renderer.domElement);

		mousemove = new app.MouseMove(cube.quaternion.w, cube.quaternion.x, cube.quaternion.y, cube.quaternion.z);
	}

	function animate() {
		requestAnimationFrame(animate);
	
		var orientation = new app.Quaternion(1, 1, 0, 0).normalize().mul(app.DeviceController.getOrientation().asQuaternion());
		var q = new THREE.Quaternion(orientation.x, orientation.y, orientation.z, orientation.w);
		cube.setRotationFromQuaternion(q);
	
		renderer.render(scene, camera);
	}

	//function animate() {
	//	requestAnimationFrame(animate);
    //
	//	var orientation = mousemove.quaternion();
	//	var q = new THREE.Quaternion(orientation.x, orientation.y, orientation.z, orientation.w);
	//	cube.setRotationFromQuaternion(q);
    //
	//	renderer.render(scene, camera);
	//}

	init();
	animate();
})(app);
